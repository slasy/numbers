﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace Numbers
{
    public readonly struct BigDouble :
        IEquatable<BigDouble>,
        IEquatable<double>,
        IEquatable<float>,
        IEquatable<long>,
        IEquatable<int>
    {
        /// <summary>
        /// https://en.wikipedia.org/wiki/Engineering_notation
        /// </summary>
        public enum Magnitude : uint
        {
            ///<summary>kilo 10^3 (1000^1)</summary>
            k = 1,
            ///<summary>mega 10^6 (1000^2)</summary>
            M,
            ///<summary>giga 10^9 (1000^3)</summary>
            G,
            ///<summary>tera 10^12 (1000^4)</summary>
            T,
            ///<summary>peta 10^15 (1000^5)</summary>
            P,
            ///<summary>exa 10^18 (1000^6)</summary>
            E,
            ///<summary>zetta 10^21 (1000^7)</summary>
            Z,
            ///<summary>yotta 10^24 (1000^8)</summary>
            Y,
        }

        /// <summary>Most significant part of number</summary>
        private readonly double _number;
        /// <summary>_number x1000^_magnitude</summary>
        private readonly Magnitude _magnitude;

        public static readonly BigDouble NaN = new BigDouble(double.NaN);
        public static readonly BigDouble PositiveInfinity = new BigDouble(double.PositiveInfinity);
        public static readonly BigDouble NegativeInfinity = new BigDouble(double.NegativeInfinity);
        public static readonly BigDouble MaxRecommendedValue = new BigDouble(999.9944444444444d, MaxRecommendedMagnitude);
        public static readonly BigDouble MinRecommendedValue = new BigDouble(-999.9944444444444d, MaxRecommendedMagnitude);
        public static readonly BigDouble MaxValue = new BigDouble(double.MaxValue, MaxMagnitude);
        public static readonly BigDouble MinValue = new BigDouble(double.MinValue, MaxMagnitude);
        public static readonly BigDouble One = new BigDouble(1);
        public static readonly BigDouble Zero = new BigDouble();
        public static readonly BigDouble MinusOne = new BigDouble(-1);
        public static readonly BigDouble Epsilon = new BigDouble(double.Epsilon);
        public const uint MaxRecommendedMagnitude = symbolCount * symbolCount + magnitudeCount;
        public const uint MaxMagnitude = uint.MaxValue - 102;

        private static readonly string s_NaN = double.NaN.ToString(CultureInfo.InvariantCulture.NumberFormat);
        private static readonly string s_InfPlus = double.PositiveInfinity.ToString(CultureInfo.InvariantCulture.NumberFormat);
        private static readonly string s_InfMinus = double.NegativeInfinity.ToString(CultureInfo.InvariantCulture.NumberFormat);
        private const Magnitude maxMagnitudeLetter = Magnitude.Y;
        private const uint symbolCount = 'z' - 'a' + 1;
        private const uint magnitudeCount = 8;

        public BigDouble(double number) : this(number, (Magnitude)0) { }
        public BigDouble(double number, uint magnitude) : this(number, (Magnitude)magnitude) { }
        public BigDouble(double number, Magnitude magnitude)
        {
            if (number == 0)
            {
                _number = 0;
                _magnitude = 0;
            }
            else if (double.IsNaN(number))
            {
                _number = double.NaN;
                _magnitude = 0;
            }
            else if (double.IsInfinity(number))
            {
                _number = number;
                _magnitude = (Magnitude)uint.MaxValue;
            }
            else if ((uint)magnitude > MaxMagnitude)
            {
                BigDouble inf;
                if (number > 0) inf = PositiveInfinity;
                else inf = NegativeInfinity;
                _number = inf._number;
                _magnitude = inf._magnitude;
            }
            else
            {
                (_number, _magnitude) = NormalizeNumber(number, magnitude);
            }
        }

        public static implicit operator BigDouble(double a) => new BigDouble(a);
        public static implicit operator BigDouble(float a) => new BigDouble(a);
        public static implicit operator BigDouble(long a) => new BigDouble(a);
        public static implicit operator BigDouble(int a) => new BigDouble(a);
        public static implicit operator BigDouble(ulong a) => new BigDouble(a);
        public static implicit operator BigDouble(uint a) => new BigDouble(a);

        public static BigDouble operator +(BigDouble a) => a;
        public static BigDouble operator -(BigDouble a) => new BigDouble(-a._number, a._magnitude);
        public static BigDouble operator +(BigDouble a, BigDouble b)
        {
            var (aNum, bNum, mag) = NormalizeNumberMagnitudes(a, b);
            return new BigDouble(aNum + bNum, mag);
        }
        public static BigDouble operator -(BigDouble a, BigDouble b)
        {
            var (aNum, bNum, mag) = NormalizeNumberMagnitudes(a, b);
            return new BigDouble(aNum - bNum, mag);
        }
        public static BigDouble operator *(BigDouble a, BigDouble b)
        {
            return new BigDouble(a._number * b._number, (uint)a._magnitude + (uint)b._magnitude);
        }
        public static BigDouble operator /(BigDouble a, BigDouble b)
        {
            if (a._magnitude < b._magnitude)
            {
                uint diff = b._magnitude - a._magnitude;
                return new BigDouble(a._number / b._number / Math.Pow(1_000, diff));
            }
            else
            {
                return new BigDouble(a._number / b._number, (uint)a._magnitude - (uint)b._magnitude);
            }
        }

        public static bool operator ==(BigDouble a, BigDouble b) => a._magnitude == b._magnitude && a._number == b._number;
        public static bool operator !=(BigDouble a, BigDouble b) => a._magnitude != b._magnitude || a._number != b._number;
        public static bool operator >(BigDouble a, BigDouble b)
        {
            if (IsNaN(a) || IsNaN(b)) return false;
            long _mA = Math.Sign(a._number) * (long)a._magnitude;
            long _mB = Math.Sign(b._number) * (long)b._magnitude;
            return _mA == _mB && a._number > b._number || _mA > _mB;
        }
        public static bool operator <(BigDouble a, BigDouble b)
        {
            if (IsNaN(a) || IsNaN(b)) return false;
            long _mA = Math.Sign(a._number) * (long)a._magnitude;
            long _mB = Math.Sign(b._number) * (long)b._magnitude;
            return _mA == _mB && a._number < b._number || _mA < _mB;
        }
        public static bool operator >=(BigDouble a, BigDouble b) => a == b || a > b;
        public static bool operator <=(BigDouble a, BigDouble b) => a == b || a < b;

        public static BigDouble Abs(BigDouble number)
        {
            return new BigDouble(Math.Abs(number._number), number._magnitude);
        }

        public static BigDouble Pow(BigDouble number, uint exponent)
        {
            return new BigDouble(Math.Pow(number._number, exponent), (uint)number._magnitude * exponent);
        }

        public static BigDouble Sqrt(BigDouble number)
        {
            uint div = (uint)number._magnitude / 2u;
            uint rem = (uint)number._magnitude % 2u;
            return new BigDouble(Math.Sqrt(number._number * Math.Pow(1_000, rem)), div);
        }

        public static int Sign(BigDouble number) => Math.Sign(number._number);

        public static BigDouble Min(BigDouble numberA, BigDouble numberB)
        {
            if (numberA < numberB || IsNaN(numberA))
            {
                return numberA;
            }
            if (numberA == numberB)
            {
                if (numberA < 0)
                {
                    return numberB;
                }
                return numberA;
            }
            return numberB;
        }

        public static BigDouble Max(BigDouble numberA, BigDouble numberB)
        {
            if (numberA > numberB || IsNaN(numberA))
            {
                return numberA;
            }
            if (numberA == numberB)
            {
                if (numberA < 0)
                {
                    return numberA;
                }
                return numberB;
            }
            return numberB;
        }

        public static bool IsNaN(BigDouble number) => double.IsNaN(number._number);
        public static bool IsFinite(BigDouble number) => IsFinite(number._number);
        public static bool IsInfinity(BigDouble number) => double.IsInfinity(number._number);
        public static bool IsNegativeInfinity(BigDouble number) => double.IsNegativeInfinity(number._number);
        public static bool IsPositiveInfinity(BigDouble number) => double.IsPositiveInfinity(number._number);

        public static long GetBase10Exponent(BigDouble number) => (long)number._magnitude * 3;
        public static long GetBase1000Exponent(BigDouble number) => (long)number._magnitude;
        public static Magnitude GetRawMagnitude(BigDouble number) => number._magnitude;
        public static double GetRawNumber(BigDouble number) => number._number;

        public static bool TryGetDouble(BigDouble bigDouble, out double justDouble)
        {
            justDouble = double.NaN;
            double exponent = Math.Pow(1000d, (double)bigDouble._magnitude);
            if (double.IsInfinity(exponent)) return false;
            justDouble = checked(bigDouble._number * exponent);
            if (double.IsInfinity(justDouble)) return false;
            else return true;
        }

        public static bool TryParse(string s, out BigDouble result)
        {
            result = NaN;
            if (s == null) return false;
            if (s == s_NaN) return true;
            if (s == s_InfPlus)
            {
                result = PositiveInfinity;
                return true;
            }
            if (s == s_InfMinus)
            {
                result = NegativeInfinity;
                return true;
            }
            int splitIndex = s.IndexOf('m');
            if (splitIndex < 1 || splitIndex - 1 == s.Length) return false;
            if (!double.TryParse(
                s.Substring(0, splitIndex),
                NumberStyles.Float | NumberStyles.Integer,
                CultureInfo.InvariantCulture.NumberFormat,
                out double number)) return false;
            if (!uint.TryParse(
                s.Substring(splitIndex + 1),
                NumberStyles.Integer,
                CultureInfo.InvariantCulture.NumberFormat,
                out uint magnitude)) return false;
            result = new BigDouble(number, magnitude);
            return true;
        }

        public static BigDouble Parse(string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException(nameof(s));
            }
            if (!TryParse(s, out BigDouble result))
            {
                throw new FormatException("Input string was not in a correct format.");
            }
            return result;
        }

        public string Serialize()
        {
            if (double.IsNaN(_number) || double.IsInfinity(_number))
            {
                return string.Format(CultureInfo.InvariantCulture.NumberFormat, "{0}", _number);
            }
            else
            {
                return string.Format(CultureInfo.InvariantCulture.NumberFormat, "{0:G17}m{1}", _number, (uint)_magnitude);
            }
        }

        public override string ToString()
        {
            if (double.IsNaN(_number) || double.IsInfinity(_number)) return _number.ToString();
            else if (_magnitude == 0) return Round(_number).ToString();
            else if (_magnitude <= maxMagnitudeLetter) return $"{Round(_number)}{_magnitude}";
            else
            {
                uint multiplier = _magnitude - maxMagnitudeLetter - 1;
                uint div = multiplier / symbolCount;
                uint mod = multiplier % symbolCount;
                if (div >= symbolCount) return "OVERFLOW";
                string combinedMagnitude = string.Empty;
                combinedMagnitude += CheckSymbolRange((char)('a' + div));
                combinedMagnitude += CheckSymbolRange((char)('a' + mod));
                return $"{Round(_number)}{combinedMagnitude}";
            }
        }

        public string ToStringEng()
        {
            if (double.IsNaN(_number) || double.IsInfinity(_number))
            {
                return _number.ToString();
            }
            else
            {
                return $"{_number:G17}x10^{(uint)_magnitude * 3}";
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is BigDouble idleNumber)
            {
                return Equals(idleNumber);
            }
            else
            {
                return false;
            }
        }

        public bool Equals(BigDouble other)
        {
            return _number == other._number && _magnitude == other._magnitude;
        }

        public bool Equals(double other)
        {
            try
            {
                double toCompare = checked(_number * Math.Pow(1_000, (double)_magnitude));
                return toCompare == other;
            }
            catch (OverflowException)
            {
                return false;
            }
        }

        public bool Equals(float other)
        {
            try
            {
                double toCompare = checked(_number * Math.Pow(1_000, (double)_magnitude));
                return toCompare == other;
            }
            catch (OverflowException)
            {
                return false;
            }
        }

        public bool Equals(long other)
        {
            try
            {
                double toCompare = checked(_number * Math.Pow(1_000, (double)_magnitude));
                return toCompare == other;
            }
            catch (OverflowException)
            {
                return false;
            }
        }

        public bool Equals(int other)
        {
            try
            {
                double toCompare = checked(_number * Math.Pow(1_000, (double)_magnitude));
                return toCompare == other;
            }
            catch (OverflowException)
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return _number.GetHashCode() ^ _magnitude.GetHashCode();
        }

        private static (double, Magnitude) NormalizeNumber(double number, Magnitude magnitude)
        {
            if (number < 1 && number > -1 && magnitude > 0)
            {
                double magnitudeToShift = Math.Min((double)magnitude, Math.Abs(Math.Floor(Math.Log(Math.Abs(number), 1_000))));
                number = checked(number * Math.Pow(1_000, magnitudeToShift));
                magnitude = checked(magnitude - (uint)magnitudeToShift);
            }
            else
            {
                double magnitudeToShift = Math.Floor(Math.Log(Round(Math.Abs(number)), 1_000));
                if (magnitudeToShift > 0)
                {
                    number = checked(number / Math.Pow(1_000, magnitudeToShift));
                    magnitude = checked(magnitude + (uint)magnitudeToShift);
                }
            }
            return (number, magnitude);
        }

        private static (double, double, Magnitude) NormalizeNumberMagnitudes(BigDouble a, BigDouble b)
        {
            if (!IsFinite(a._number) || !IsFinite(b._number) || a._magnitude == b._magnitude)
            {
                return (a._number, b._number, (Magnitude)Math.Max((uint)a._magnitude, (uint)b._magnitude));
            }
            if (a._magnitude < b._magnitude)
            {
                return (a._number / Math.Pow(1_000, b._magnitude - a._magnitude), b._number, b._magnitude);
            }
            else if (a._magnitude > b._magnitude)
            {
                return (a._number, b._number / Math.Pow(1_000, a._magnitude - b._magnitude), a._magnitude);
            }
            throw new ArgumentOutOfRangeException("Unable to normalize magnitudes");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsFinite(double number)
        {
            return !double.IsInfinity(number)
                && !double.IsNaN(number);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static char CheckSymbolRange(char symbol, char min = 'a', char max = 'z')
        {
            if (symbol - min < 0) throw new ArgumentOutOfRangeException("Below min");
            if (symbol - max > 0) throw new ArgumentOutOfRangeException("Above max");
            return symbol;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static double Round(double number)
        {
            return Math.Round(number, 2, MidpointRounding.AwayFromZero);
        }
    }
}
