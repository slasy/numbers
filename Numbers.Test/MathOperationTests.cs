using System;
using System.Collections;
using NUnit.Framework;

namespace Numbers.Test
{
    public class MathOperationTests
    {
        // - DATA GENERATORS ------------------------------------------------------------------------------------------
        private static IEnumerable SourceDataGenerator(
            Func<double, BigDouble> inA,
            Func<double, BigDouble> inB,
            Func<double, double, BigDouble> mathOut)
        {
            for (int a = -500; a < 500; a += 50)
            {
                for (int b = -500; b < 500; b += 100)
                {
                    yield return new TestCaseData(inA?.Invoke(a) ?? new BigDouble(a), inB?.Invoke(b) ?? new BigDouble(b))
                        .Returns(mathOut(a, b));
                }
            }
        }
        private static IEnumerable AddSourceData() => SourceDataGenerator(null, null, (a, b) => new BigDouble(a + b));
        private static IEnumerable AddSourceDataDiffMagnitude() => SourceDataGenerator(
            null,
            b => new BigDouble(b, BigDouble.Magnitude.k),
            (a, b) => new BigDouble(a + b * 1_000));
        private static IEnumerable SubSourceData1() => SourceDataGenerator(null, null, (a, b) => new BigDouble(a - b));
        private static IEnumerable SubSourceData2() => SourceDataGenerator(
            a => new BigDouble(a, BigDouble.Magnitude.T),
            b => new BigDouble(b, BigDouble.Magnitude.T),
            (a, b) => new BigDouble(a - b, BigDouble.Magnitude.T));
        private static IEnumerable SubSourceData3() => SourceDataGenerator(
            a => new BigDouble(a, BigDouble.Magnitude.G),
            b => new BigDouble(b, BigDouble.Magnitude.T),
            (a, b) => new BigDouble((a / 1_000) - b, BigDouble.Magnitude.T));
        private static IEnumerable SubSourceData4() => SourceDataGenerator(
            a => new BigDouble(a, BigDouble.Magnitude.T),
            b => new BigDouble(b, BigDouble.Magnitude.G),
            (a, b) => new BigDouble(a - (b / 1_000), BigDouble.Magnitude.T));
        private static IEnumerable MulSourceData() => SourceDataGenerator(null, null, (a, b) => new BigDouble(a * b));
        private static IEnumerable MulSourceDataDiffMagnitude1()
        {
            for (int a = -100; a < 100; a++)
            {
                for (int b = -100; b < 100; b++)
                {
                    yield return new TestCaseData(new BigDouble(a, BigDouble.Magnitude.M), new BigDouble(b))
                        .Returns(new BigDouble(a * b, BigDouble.Magnitude.M));
                }
            }
        }
        private static IEnumerable MulSourceDataDiffMagnitude2()
        {
            for (int a = -100; a < 100; a++)
            {
                for (int b = -100; b < 100; b++)
                {
                    yield return new TestCaseData(new BigDouble(a, BigDouble.Magnitude.k), new BigDouble(b, BigDouble.Magnitude.T))
                        .Returns(new BigDouble(a * b, (uint)BigDouble.Magnitude.k + (uint)BigDouble.Magnitude.T));
                }
            }
        }
        private static IEnumerable DivSourceData() => SourceDataGenerator(
            null,
            b => new BigDouble(b == 0 ? 1 : b),
            (a, b) => new BigDouble(a / (b == 0 ? 1 : b)));
        private static IEnumerable DivSourceDataDiffMagnitude1()
        {
            for (int a = -100; a < 100; a++)
            {
                for (int b = -100; b < 100; b++)
                {
                    if (b == 0) continue;
                    yield return new TestCaseData(new BigDouble(a, BigDouble.Magnitude.T), new BigDouble(b))
                        .Returns(new BigDouble((double)a / b, BigDouble.Magnitude.T));
                }
            }
        }
        private static IEnumerable DivSourceDataDiffMagnitude2()
        {
            for (int a = -100; a < 100; a++)
            {
                for (int b = -10; b < 10; b++)
                {
                    if (b == 0) continue;
                    yield return new TestCaseData(new BigDouble(a, BigDouble.Magnitude.k), new BigDouble(b, BigDouble.Magnitude.T))
                        .Returns(new BigDouble((double)a / b / 1_000_000_000D));
                }
            }
        }
        private static IEnumerable NaNData()
        {
            yield return new TestCaseData(BigDouble.NaN, (BigDouble)10);
            yield return new TestCaseData(BigDouble.NaN, (BigDouble)0);
            yield return new TestCaseData((BigDouble)10, BigDouble.NaN);
            yield return new TestCaseData((BigDouble)0, BigDouble.NaN);
            yield return new TestCaseData(BigDouble.NaN, BigDouble.PositiveInfinity);
            yield return new TestCaseData(BigDouble.NaN, BigDouble.NegativeInfinity);
            yield return new TestCaseData(BigDouble.NegativeInfinity, BigDouble.NaN);
            yield return new TestCaseData(BigDouble.PositiveInfinity, BigDouble.NaN);
        }

        // - TESTS ----------------------------------------------------------------------------------------------------

        [TestCaseSource(nameof(AddSourceData))]
        [TestCaseSource(nameof(AddSourceDataDiffMagnitude))]
        public BigDouble AddTwoNumbers(BigDouble a, BigDouble b)
        {
            var c = a + b;
            TestContext.WriteLine($"{a} + {b} = {c}");
            return c;
        }

        [TestCaseSource(nameof(SubSourceData1))]
        [TestCaseSource(nameof(SubSourceData2))]
        [TestCaseSource(nameof(SubSourceData3))]
        [TestCaseSource(nameof(SubSourceData4))]
        public BigDouble SubTwoNumbers(BigDouble a, BigDouble b)
        {
            var c = a - b;
            TestContext.WriteLine($"{a} - {b} = {c}");
            return c;
        }

        [TestCaseSource(nameof(MulSourceData))]
        [TestCaseSource(nameof(MulSourceDataDiffMagnitude1))]
        [TestCaseSource(nameof(MulSourceDataDiffMagnitude2))]
        public BigDouble MulTwoNumbers(BigDouble a, BigDouble b)
        {
            var c = a * b;
            TestContext.WriteLine($"{a} * {b} = {c}");
            return c;
        }

        [TestCaseSource(nameof(DivSourceData))]
        [TestCaseSource(nameof(DivSourceDataDiffMagnitude1))]
        [TestCaseSource(nameof(DivSourceDataDiffMagnitude2))]
        public BigDouble DivTwoNumbers(BigDouble a, BigDouble b)
        {
            var c = a / b;
            TestContext.WriteLine($"{a} / {b} = {c}");
            return c;
        }

        [Test]
        public void DivByZero()
        {
            Assert.IsTrue(BigDouble.IsPositiveInfinity(new BigDouble(Math.E) / 0));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(new BigDouble(-Math.PI) / 0));
            Assert.IsTrue(BigDouble.IsInfinity(new BigDouble(Math.E) / BigDouble.Epsilon));
        }

        [Test]
        public void MulToOverflow()
        {
            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.MaxValue * BigDouble.MaxValue));
            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.MaxValue * -BigDouble.MinValue));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.MaxValue * BigDouble.MinValue));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.MaxValue * -BigDouble.MaxValue));
        }

        [Test]
        public void AddSubInfinity()
        {
            Assert.IsTrue(BigDouble.IsPositiveInfinity(123456789 + BigDouble.PositiveInfinity));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(123456789 - BigDouble.PositiveInfinity));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(123456789 + BigDouble.NegativeInfinity));
            Assert.IsTrue(BigDouble.IsPositiveInfinity(123456789 - BigDouble.NegativeInfinity));

            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.PositiveInfinity + 123456789));
            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.PositiveInfinity - 123456789));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.NegativeInfinity + 123456789));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.NegativeInfinity - 123456789));

            Assert.IsTrue(BigDouble.IsNaN(BigDouble.PositiveInfinity + BigDouble.NegativeInfinity));
            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.PositiveInfinity - BigDouble.NegativeInfinity));
            Assert.IsTrue(BigDouble.IsNaN(BigDouble.NegativeInfinity + BigDouble.PositiveInfinity));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.NegativeInfinity - BigDouble.PositiveInfinity));
        }

        [Test]
        public void MulDivInfinity()
        {
            Assert.IsTrue(BigDouble.IsPositiveInfinity(123456789 * BigDouble.PositiveInfinity));
            Assert.AreEqual(0, 123456789 / BigDouble.PositiveInfinity);
            Assert.IsTrue(BigDouble.IsNegativeInfinity(123456789 * BigDouble.NegativeInfinity));
            Assert.AreEqual(0, 123456789 / BigDouble.NegativeInfinity);

            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.PositiveInfinity * 123456789));
            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.PositiveInfinity / 123456789));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.NegativeInfinity * 123456789));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.NegativeInfinity / 123456789));

            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.PositiveInfinity * BigDouble.NegativeInfinity));
            Assert.IsTrue(BigDouble.IsNaN(BigDouble.PositiveInfinity / BigDouble.NegativeInfinity));
            Assert.IsTrue(BigDouble.IsNegativeInfinity(BigDouble.NegativeInfinity * BigDouble.PositiveInfinity));
            Assert.IsTrue(BigDouble.IsNaN(BigDouble.NegativeInfinity / BigDouble.PositiveInfinity));

            Assert.IsTrue(BigDouble.IsPositiveInfinity(BigDouble.PositiveInfinity * BigDouble.PositiveInfinity));
            Assert.IsTrue(BigDouble.IsNaN(BigDouble.NegativeInfinity / BigDouble.NegativeInfinity));
            Assert.IsTrue(BigDouble.IsNaN(BigDouble.PositiveInfinity / BigDouble.PositiveInfinity));
        }

        [TestCaseSource(nameof(NaNData))]
        public void OperationsWithNaN(BigDouble a, BigDouble b)
        {
            Assert.IsTrue(BigDouble.IsNaN(a + b));
            Assert.IsTrue(BigDouble.IsNaN(a - b));
            Assert.IsTrue(BigDouble.IsNaN(a * b));
            Assert.IsTrue(BigDouble.IsNaN(a / b));
        }

        [Test]
        public void Power()
        {
            Assert.AreEqual(1, BigDouble.Pow(BigDouble.Zero, 0));
            Assert.AreEqual(1, BigDouble.Pow(1, 0));
            Assert.AreEqual(1, BigDouble.Pow(1234, 0));
            Assert.AreEqual(1, BigDouble.Pow(new BigDouble(123, 100), 0));

            Assert.AreEqual(
                new BigDouble(123, BigDouble.Magnitude.M) * new BigDouble(123, BigDouble.Magnitude.M),
                BigDouble.Pow(new BigDouble(123, BigDouble.Magnitude.M), 2));
            Assert.AreEqual(BigDouble.MaxRecommendedValue, BigDouble.Pow(BigDouble.MaxRecommendedValue, 1));
            Assert.AreEqual(BigDouble.MinRecommendedValue, BigDouble.Pow(BigDouble.MinRecommendedValue, 1));
        }

        [Test]
        public void SquareRoot()
        {
            Assert.AreEqual(3, BigDouble.Sqrt(new BigDouble(9)));
            Assert.AreEqual(40, BigDouble.Sqrt(new BigDouble(1.6, BigDouble.Magnitude.k)));
            Assert.AreEqual(2200, BigDouble.Sqrt(new BigDouble(4.84, BigDouble.Magnitude.M)));
            Assert.AreEqual(33000, BigDouble.Sqrt(new BigDouble(1.089, BigDouble.Magnitude.G)));
            Assert.AreEqual(
                new BigDouble(1.23, BigDouble.Magnitude.T),
                BigDouble.Sqrt(new BigDouble(1.5129, BigDouble.Magnitude.Y)));
            Assert.AreEqual(
                new BigDouble(444, BigDouble.Magnitude.P),
                BigDouble.Sqrt(new BigDouble(197.136, 11)));
            Assert.AreEqual(
                new BigDouble(140.40512811147605, BigDouble.Magnitude.P),
                BigDouble.Sqrt(new BigDouble(19.7136, 11)));
            Assert.AreEqual(
                new BigDouble(140.40512811147605, BigDouble.Magnitude.P),
                BigDouble.Sqrt(new BigDouble(19.7136, 11)));
            Assert.AreEqual(
                new BigDouble(1.4040512811147605, BigDouble.Magnitude.E),
                BigDouble.Sqrt(new BigDouble(1971.36, 11)));
        }

        [Test]
        public void Absolute()
        {
            Assert.AreEqual(123, BigDouble.Abs(+123));
            Assert.AreEqual(123, BigDouble.Abs(123));
            Assert.AreEqual(123, BigDouble.Abs(-123));
            Assert.AreEqual(0, BigDouble.Abs(-0));
            Assert.AreEqual(0, BigDouble.Abs(0));
            Assert.AreEqual(0, BigDouble.Abs(+0));
        }

        [TestCase(1d, 0d)]
        [TestCase(1d, -1d)]
        [TestCase(1_000d, 0d)]
        [TestCase(5_000d, 1_000d)]
        [TestCase(5_001d, 5_000d)]
        [TestCase(123_000d, 123d)]
        [TestCase(5_001d, -5_002d)]
        [TestCase(-5_000d, -5_001d)]
        [TestCase(-1d, -5_001d)]
        [TestCase(double.PositiveInfinity, 0d)]
        [TestCase(double.PositiveInfinity, double.MaxValue)]
        [TestCase(double.PositiveInfinity, double.NegativeInfinity)]
        public void Comparison_A_isGreaterThan_B(double A, double B)
        {
            BigDouble a = A;
            BigDouble b = B;

            Assert.IsTrue(a > b);
            Assert.IsFalse(a < b);
            Assert.IsTrue(b < a);
            Assert.IsFalse(b > a);

            Assert.IsTrue(a != b);
            Assert.IsFalse(a == b);
            Assert.IsTrue(a >= b);
            Assert.IsFalse(a <= b);
            Assert.IsTrue(b <= a);
            Assert.IsFalse(b >= a);

            Assert.IsTrue(a != BigDouble.NaN);
            Assert.IsTrue(b != BigDouble.NaN);
            Assert.IsFalse(a == BigDouble.NaN);
            Assert.IsFalse(b == BigDouble.NaN);
            Assert.IsFalse(a > BigDouble.NaN);
            Assert.IsFalse(a < BigDouble.NaN);
            Assert.IsFalse(b > BigDouble.NaN);
            Assert.IsFalse(b < BigDouble.NaN);
            Assert.IsFalse(a <= BigDouble.NaN);
            Assert.IsFalse(b <= BigDouble.NaN);
            Assert.IsFalse(a >= BigDouble.NaN);
            Assert.IsFalse(b >= BigDouble.NaN);
        }

        [Test]
        public void ComprisonOfNaN()
        {
            Assert.IsFalse(BigDouble.NaN > BigDouble.Zero);
            Assert.IsFalse(BigDouble.NaN < BigDouble.Zero);
            Assert.IsFalse(BigDouble.NaN == BigDouble.Zero);
            Assert.IsTrue(BigDouble.NaN != BigDouble.Zero);
            Assert.IsFalse(BigDouble.NaN >= BigDouble.Zero);
            Assert.IsFalse(BigDouble.NaN <= BigDouble.Zero);
#pragma warning disable CS1718
            Assert.IsFalse(BigDouble.NaN > BigDouble.NaN);
            Assert.IsFalse(BigDouble.NaN < BigDouble.NaN);
            Assert.IsFalse(BigDouble.NaN == BigDouble.NaN);
            Assert.IsTrue(BigDouble.NaN != BigDouble.NaN);
            Assert.IsFalse(BigDouble.NaN >= BigDouble.NaN);
            Assert.IsFalse(BigDouble.NaN <= BigDouble.NaN);
#pragma warning restore
            Assert.IsFalse(BigDouble.NaN > BigDouble.PositiveInfinity);
            Assert.IsFalse(BigDouble.NaN < BigDouble.PositiveInfinity);
            Assert.IsFalse(BigDouble.NaN == BigDouble.PositiveInfinity);
            Assert.IsTrue(BigDouble.NaN != BigDouble.PositiveInfinity);
            Assert.IsFalse(BigDouble.NaN >= BigDouble.PositiveInfinity);
            Assert.IsFalse(BigDouble.NaN <= BigDouble.PositiveInfinity);

            Assert.IsFalse(BigDouble.NaN > BigDouble.NegativeInfinity);
            Assert.IsFalse(BigDouble.NaN < BigDouble.NegativeInfinity);
            Assert.IsFalse(BigDouble.NaN == BigDouble.NegativeInfinity);
            Assert.IsTrue(BigDouble.NaN != BigDouble.NegativeInfinity);
            Assert.IsFalse(BigDouble.NaN >= BigDouble.NegativeInfinity);
            Assert.IsFalse(BigDouble.NaN <= BigDouble.NegativeInfinity);
        }

        [Test]
        public void MinTest()
        {
            Assert.AreEqual(new BigDouble(123), BigDouble.Min(123, 124));
            Assert.AreEqual(new BigDouble(-123), BigDouble.Min(-123, 124));
            Assert.AreEqual(new BigDouble(123), BigDouble.Min(124, 123));
            Assert.AreEqual(new BigDouble(-124), BigDouble.Min(-124, 123));
            Assert.AreEqual(new BigDouble(124), BigDouble.Min(new BigDouble(123, 1), 124));
            Assert.AreEqual(new BigDouble(124), BigDouble.Min(124, new BigDouble(123, 1)));
            Assert.AreEqual(BigDouble.NegativeInfinity, BigDouble.Min(BigDouble.PositiveInfinity, BigDouble.NegativeInfinity));
            Assert.AreEqual(BigDouble.Zero, BigDouble.Min(0, BigDouble.PositiveInfinity));
            Assert.AreEqual(BigDouble.NegativeInfinity, BigDouble.Min(0, BigDouble.NegativeInfinity));
            Assert.AreEqual(BigDouble.Zero, BigDouble.Min(BigDouble.PositiveInfinity, 0));
            Assert.AreEqual(BigDouble.NegativeInfinity, BigDouble.Min(BigDouble.NegativeInfinity, 0));
            Assert.AreEqual(BigDouble.NegativeInfinity, BigDouble.Min(BigDouble.NegativeInfinity, -100));
            Assert.AreEqual(BigDouble.NegativeInfinity, BigDouble.Min(BigDouble.NegativeInfinity, 100));
        }

        [Test]
        public void MaxTest()
        {
            Assert.AreEqual(new BigDouble(124), BigDouble.Max(123, 124));
            Assert.AreEqual(new BigDouble(124), BigDouble.Max(-123, 124));
            Assert.AreEqual(new BigDouble(124), BigDouble.Max(124, 123));
            Assert.AreEqual(new BigDouble(123), BigDouble.Max(-124, 123));
            Assert.AreEqual(new BigDouble(123, 1), BigDouble.Max(new BigDouble(123, 1), 124));
            Assert.AreEqual(new BigDouble(123, 1), BigDouble.Max(124, new BigDouble(123, 1)));
            Assert.AreEqual(BigDouble.PositiveInfinity, BigDouble.Max(BigDouble.PositiveInfinity, BigDouble.NegativeInfinity));
            Assert.AreEqual(BigDouble.PositiveInfinity, BigDouble.Max(0, BigDouble.PositiveInfinity));
            Assert.AreEqual(BigDouble.Zero, BigDouble.Max(0, BigDouble.NegativeInfinity));
            Assert.AreEqual(BigDouble.PositiveInfinity, BigDouble.Max(BigDouble.PositiveInfinity, 0));
            Assert.AreEqual(BigDouble.Zero, BigDouble.Max(BigDouble.NegativeInfinity, 0));
            Assert.AreEqual(new BigDouble(-100), BigDouble.Max(BigDouble.NegativeInfinity, -100));
            Assert.AreEqual(new BigDouble(100), BigDouble.Max(BigDouble.NegativeInfinity, 100));
        }

        [Test]
        public void SignTest()
        {
            Assert.AreEqual(1, BigDouble.Sign(1));
            Assert.AreEqual(1, BigDouble.Sign(1234567890));
            Assert.AreEqual(1, BigDouble.Sign(.0000001));
            Assert.AreEqual(1, BigDouble.Sign(new BigDouble(.0000001, 10)));
            Assert.AreEqual(1, BigDouble.Sign(BigDouble.Epsilon));
            Assert.AreEqual(0, BigDouble.Sign(0));
            Assert.AreEqual(0, BigDouble.Sign(BigDouble.Zero));
            Assert.AreEqual(0, BigDouble.Sign(new BigDouble(0, 666)));
            Assert.AreEqual(-1, BigDouble.Sign(-1));
            Assert.AreEqual(-1, BigDouble.Sign(-1234567890));
            Assert.AreEqual(-1, BigDouble.Sign(-.0000001));
            Assert.AreEqual(-1, BigDouble.Sign(new BigDouble(-.0000001, 10)));
            Assert.AreEqual(-1, BigDouble.Sign(-BigDouble.Epsilon));
        }
    }
}
